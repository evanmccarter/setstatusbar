# setstatusbar
A modular status bar for dwm
![screenshot](sshot.png)
[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/J3J11S7DZ)
## Table of Contents
- [Current Functions](#current-functions)
  - [printalsa](#printalsa)
  - [printpulse](#printpulse)
  - [printbattery](#printbattery)
  - [printcountdown](#printcountdown)
  - [printalarm](#printalarm)
  - [printkeyboard](#printkeyboard)
  - [printresources](#printresources)
  - [printcmus](#printcmus)
  - [printmpc](#printmpc)
  - [printspotify](#printmpc)
  - [printdate](#printdate)
  - [printmail](#printmail)
  - [printweather](#printweather)
  - [printnetworkmanager](#printnetworkmanager)
  - [printwpa](#printwpa)
  - [printvpn](#printvpn)
  - [printccurse](#printccurse)
  - [printtransmission](#printtransmission)
  - [printbacklight](#printbacklight)
  - [printconnman](#printconnman)
  - [printloadavg](#printloadavg)
- [Installation](#installation)
- [Recommendations](#recommendations)
- [Usage](#usage)
- [Customizing](#customizing)
- [Contributing](#contributing)
- [Acknowledgements](#acknowledgements)
### printalsa
Displays the current master volume of ALSA
```
[🔉 55%]
```
Dependencies: ```alsa-utils```
### printpulse
Displays the current master volume of PulseAudio
```
[🔉 55%]
```
Dependencies: ```pamixer```
### printbattery
Displays battery level and status
```
[🔋 100% full]
```
### printcountdown
Displays the status of [countdown](https://github.com/joestandring/countdown)
```
[⏳ 00:10:00]
```
Dependencies: ```countdown.sh```
### printalarm
Displays upcoming alarms from [alarm](https://github.com/joestandring/alarm)
```
[⏰ 22:30:00]
```
Dependencies: ```alarm.sh```
### printkeyboard
Displays the current keyboard layout
```
[⌨ gb]
```
Dependencies: ```xorg-setxkbmap```
### printresources
Displays information regarding memory, CPU temperature, and storage
```
[🖥 MEM 1.3Gi/15Gi CPU 45C STO 2.3G/200G: 2%]
```
### printcmus
Displays current cmus status, artist, track, position, duration, and shuffle
```
[▶ The Unicorns - Tuff Ghost 0:43/2:56 🔀]
```
Dependencies: ```cmus```
### printmpc
Displays current mpc status, artist, track, position, duration, and shuffle
```
[▶ The Unicorns - Tuff Ghost 0:43/2:56 🔀]
```
Dependencies: ```mpc```
### printspotify
Displays current Spotify status, artist, track, and duration

Unfortunatley a method to display the track position and shuffle status have not been found
```
[▶ The Unicorns - Tuff Ghost 2:56]
```
Dependencies: ```spotify, playerctl```

### printdate
Displays the current date and time
```
[🕰 Mon 06-05-19 21:31:58]
```
### printmail
Displays the current number of emails in an inbox
```
[📫 2]
```
### printweather
Displays the current weather provided by [wttr.in](https://wttr.in)
```
[☀ +20°C]
```
### printnetworkmanager
Displays the current network connection, private IP, and public IP using NetworkManager
```
[🌐 enp7s0: 192.168.0.1/24 | 185.199.109.153]
```
Dependencies: ```NetworkManager, curl```
### printwpa
Displays the current network connection and private IP using wpa_cli
```
[襤 My-Wifi 192.168.0.3]
```
Dependancies: ```wpa_cli```
### printvpn
Displays the current VPN connection
```
[🔒 Sweden - Stockholm]
```
Dependencies: ```NetworkManager-openvpn```
### printccurse
Displays the next appointment from calcurse
```
[💡 18/04/19 19:00 20:00 Upload printccurse]
```
Dependencies: ```calcurse```
### printtransmission
Displays the current status of a torrent with transmission-remote
```
[⏬ archlinux-2019.06.01... | 92% 1min ⬆3.4 ⬇1.5]
```
Dependencies: ```transmission-remote```
### printbacklight
Displays the current backlight level with xbacklight
```
[☀ 80]
```
Dependencies: ```xbacklight```
### printconnman
Shows network information IP, SSID, WLan strength (if connected to WLan) using connman.
```
[🌐 192.169.189.12 HomeNetworkName 53%]
```
Dependencies: ```connman```
### printloadavg
Displays the average system load
```
[⏱ 0.14 0.17 0.18]
```
## Installation
1. Clone and enter the repository:
```
$ git clone https://gitlab.com/evanmccarter/setstatusbar.git
$ cd setstatusbar
```
2. Make the script executable
```
$ chmod +x setstatusbar
```
## Recommendations
To make the most out of unicode support, consider using a font that inludes many unicode charachters. For example:
* [Nerd Fonts](https://github.com/ryanoasis/nerd-fonts)
* [Siji](https://github.com/stark/siji)
* [Font Awesome](https://fontawesome.com/)

While not always neccessary, it's a good idea to specify these fonts in your dwm config.
## Quick Start
Simply run the script and dwm should display your bar:
```
$ ./setstatusbar
```
Most likely, you will need to change some values for functions to get them to work - these are outlined with a comment for functions where this is likely the case.
If you would like your bar to be displayed when X starts, add this to your .xinitrc file before launching dwm. For example, if the script is located in /home/$USER/setstatusbar/:
```
# Statusbar
/home/$USER/setstatusbar/setstatusbar &

# Start dwm
exec dwm
```
## Customizing
setstatusbar is completley modular, meaning you can mix and match functions to your hearts content. Its functions are located in the functions/ subdirectory and included in setstatusbar
If you want to make your own function, for example printmyfunction, you should create it in the functions/ subdirectory before including it in setstatusbar and adding it to the xsetroot command:
```
# Import the modules
. "$DIR/functions/printmyfunction"

while true
do
	xsetroot -name "$(printmyfunction)"
	sleep 1
done
```
You can also decide to use unicode or plaintext identifiers for functions by altering the ```$UNICODE``` value. For example, set to ```"unicode"```, ```printmail``` will display:
```
[📫 0]
```
Whereas, if it is not set it will display:
```
[MAIL 0]
```
## Contributing
See [CONTRIBUTING.md](CONTRIBUTING.md) before contributing.
## Acknowledgements
Code for some functions was modified from:
* Klemens Nanni: https://notabug.org/kl3
* https://github.com/boylemic/configs/blob/master/dwm_status
* Parket Johnson: https://github.com/ronno/scripts/blob/master/xsetcmus
* https://dwm.suckless.org/status_monitor
* https://github.com/mcallistertyler95/dwm-bar
* https://github.com/joestandring/dwm-bar
