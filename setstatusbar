#!/bin/sh

# A modular status bar for dwm
# Joe Standring <git@joestandring.com>
# GNU GPLv3

# Dependencies: xorg-xsetroot

# Import functions with "$include /route/to/module"
# It is recommended that you place functions in the subdirectory ./functions and use: . "$DIR/functions/printexample"

# Store the directory the script is running from
LOC="$(readlink -f "$0")"
DIR="$(dirname "$LOC")"

# Change the appearance of the module identifier. If this is set to "unicode", then symbols will be used as identifiers instead of text. E.g. [📪 0] instead of [MAIL 0].
# Requires a font with adequate unicode character support
export UNICODE="${UNICODE-unicode}"

# Change the character(s) used to separate modules. If two are used, they will be placed at the start and end.
if [ "$UNICODE" = "unicode" ]; then
	SEP=""
else
	SEP="|"
fi


MODULES=""
MODULES="$MODULES printdate"
MODULES="$MODULES printcountdown"
MODULES="$MODULES printalarm"
MODULES="$MODULES printccurse"
MODULES="$MODULES printbattery"
MODULES="$MODULES printbacklight"
MODULES="$MODULES printpulseaudio"
MODULES="$MODULES printalsa"
MODULES="$MODULES printkeyboard"
MODULES="$MODULES printvpn"
MODULES="$MODULES printmail"
MODULES="$MODULES printtransmission"
MODULES="$MODULES printweather"
MODULES="$MODULES printcmus"
MODULES="$MODULES printmpc"
MODULES="$MODULES printspotify"
#MODULES="$MODULES printnetworkmanager"
MODULES="$MODULES printconnman"
MODULES="$MODULES printresources"
#MODULES="$MODULES printloadavg"

# Import the modules

for i in $MODULES; do
	. "$DIR/functions/$i"
done

# Update status bar every second
while sleep 1
do
	TEXT=""


	for i in $MODULES; do
		RESULT="$($i)"
		if [ $? = 0 ] && [ "$RESULT" ] && [ ${#RESULT} -lt 64 ]; then
			if [ "$TEXT" ]; then
				TEXT="$SEP$TEXT"
			fi
			TEXT="$RESULT$TEXT"
		fi
	done


	if [ "$UNICODE" = "unicode" ]; then
		TEXT="$(echo "$TEXT" | tr -d ' ')"
	fi

	xsetroot -name "$TEXT"

	echo "Status bar:" >&3
	echo "$TEXT"


	if [ "$STOPSTATUSBAR" ]; then
		break
	fi
done
